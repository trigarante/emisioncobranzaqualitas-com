const pkg = require('./package')

module.exports = {
  server: {
    // nuxt.js server options ( can be overrided by environment variables )
    port: 3200,
    host: "127.0.0.1",
  },
  mode: 'universal',

  /*
  ** Headers of the page
  */
    head: {
        title: 'Emitir y Cobrar - Seguros de Auto - Seguros de Autos - AhorraSeguros®',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description',name: 'description',content: 'El Mejor Comparador de Seguros de Autos en México. ✓ Hasta 50%- al Comparar Seguro de Auto ✓ Compara 19 Aseguradoras en 30 Segundos con AhorraSeguros®.'},
            {hid: 'keywords',name: 'keywords',content: '"comparador de seguros de autos, comparador de seguro de auto, comparador seguros autos, Comparar Seguros, Comparar Seguros de Autos, Compara Seguros, Compara Seguros de Autos, Comparar Seguros en México, Compara Seguros CDMX, Comparar Seguros de Autos en México, Comparador de Seguros'},
            {hid: 'author', name: 'author', content: 'Ahorra Seguros'},
            {hid: 'robots', name: 'robots', content: 'index, follow'}
        ],
        link: [
            {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Open+Sans:400,700'},
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
            {rel: 'sitemap', type: 'application/xml', href: '/sitemap.xml'},
            {rel: 'canonical', href: 'https://www.ahorraseguros.mx/seguros-de-autos/comparar-seguros-de-autos/'},
        ],
        script: [
            { src: 'https://nosir.github.io/cleave.js/dist/cleave.min.js' }
        ],
    },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [{src: '~/plugins/filters.js'}],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt',
    '@nuxtjs/font-awesome',
    [
      'nuxt-fontawesome',
      {
        component: 'fa',
        imports: [
          {
            set: '@fortawesome/free-solid-svg-icons',
            icons: ['fas']
          }
        ]
      }
    ],
    [
      'nuxt-validate',
      {
        lang: 'es',
        locale: 'es',
      }
    ],
    ['@nuxtjs/moment', ['es']]
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  env: {
    urlWSAutos: 'https://ahorraseguros.mx/ws-autos/servicios',
    //urlWSAutos: 'http://192.168.10.65:8080/ws-autos/servicios',
    urlDB: 'https://ahorraseguros.mx/ws-rest/servicios',
    //urlDB: 'http://192.168.10.65:8080/ws-rest/servicios',
    urlNewDataBase: 'https://www.ahorraseguros.mx/servicios'
  },
  //router: {
  //      base: '/compras-online/'
  //},
  render: {
      http2: {push: true},
      resourceHints: false,
      gzip: {threshold: 9}
  },
}
