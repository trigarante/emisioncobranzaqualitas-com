import axios from 'axios'

const smsService = {}

smsService.sendSms = function(data) {
  return axios({
    method: 'post',
    headers:{'Content-Type': 'application/json'},
    url: process.env.urlDB + '/sendSms',
    data:  data
  })
    .then(res => res.data)
    .catch(err =>
      console.error('Ups... no se pudo enviar el SMS :( ' + err)
    )
}
export default smsService
