import axios from 'axios'

const emisionService = {}

emisionService.savePago = function(peticion) {
  return axios({
    method: 'post',
    headers:{'Content-Type': 'application/json'},
    url: process.env.urlDB + '/savePago',
    data:  peticion
  })
    .then(res => res.data)
    .catch(err =>
      console.error('Ups... no pudo guardar el pago :( ' + err)
    )
}
export default emisionService
