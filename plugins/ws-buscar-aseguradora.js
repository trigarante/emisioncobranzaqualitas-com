import axios from 'axios'

const wsFindAseguradora = {}

wsFindAseguradora.getDataAseguradora = function(aseguradora) {
  return axios({
    method: 'get',
    url: process.env.urlDB + '/find-aseguradora/' + aseguradora
  })
    .then(res => res.data)
    .catch(err =>
      console.error('Ups... Error al buscar la aseguradora... :( ' + err)
    )
}
export default wsFindAseguradora
