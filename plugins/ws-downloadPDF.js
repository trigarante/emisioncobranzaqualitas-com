import baseWs from './ws-db'

const downloadDocumentService = {}

downloadDocumentService.downloadFilePDF = function(docs,aseguradora) {
  return baseWs
      .get('/descarga', {
        params: { docs, aseguradora }
      })
      .then(res => res.data)
      .catch(err =>
          console.error(
              'Ups... hubo un problema al descargar los documentos...' + err
          )
      )
}
export default downloadDocumentService
