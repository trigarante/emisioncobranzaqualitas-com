import axios from 'axios'

const emisionService = {}

emisionService.saveEmisionRespuesta= function(peticion, id) {
  return axios({
    method: 'post',
    headers:{'Content-Type': 'application/json'},
    url: process.env.urlDB + '/update-emision/'+id,
    data:  peticion
  })
    .then(res => res.data)
    .catch(err =>
      console.error('Ups... no guardaron los datos de la emision :( ' + err)
    )
}
export default emisionService
